﻿using System.Collections.Generic; 

namespace Lection3.Common.Entities
{
    public class InitialData
    {
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }
        public InitialData() 
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            Teams = new List<Team>();
            Users = new List<User>();
        }
    }
}
