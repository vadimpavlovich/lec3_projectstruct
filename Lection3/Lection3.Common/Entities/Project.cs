﻿using Lection3.Common.Interfaces;
using System;

namespace Lection3.Common.Entities
{
    public class Project : BaseEntity
    { 
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    } 
}
