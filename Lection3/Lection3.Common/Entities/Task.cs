﻿using System;
using Lection3.Common.Enums;
using Lection3.Common.Interfaces;

namespace Lection3.Common.Entities
{
    public class Task : BaseEntity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskSatate State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; } 
    }
}
