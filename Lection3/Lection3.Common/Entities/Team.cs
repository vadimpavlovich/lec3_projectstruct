﻿using Lection3.Common.Interfaces;
using System;

namespace Lection3.Common.Entities
{
    public class Team : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; } 
    }
}
