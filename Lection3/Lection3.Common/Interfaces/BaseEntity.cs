﻿namespace Lection3.Common.Interfaces
{
    public interface BaseEntity
    { 
        public int Id { get; set; } 
    }
}
