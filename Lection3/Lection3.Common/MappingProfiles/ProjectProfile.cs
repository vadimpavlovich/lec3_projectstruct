﻿using AutoMapper;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.Common.Entities;

namespace Lection3.Common.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>(); 
            CreateMap<UpdateProjectDTO, Project>(); 
            CreateMap<CreateProjectDTO, Project>(); 
        }
    }
}
