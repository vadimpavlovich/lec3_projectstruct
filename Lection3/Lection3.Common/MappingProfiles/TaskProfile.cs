﻿using AutoMapper;
using Lection3.Common.ModelsDTO.WebAPI; 
using Task = Lection3.Common.Entities.Task;

namespace Lection3.Common.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<UpdateTaskDTO, Task>();
            CreateMap<CreateTaskDTO, Task>();
        }
    }
}
