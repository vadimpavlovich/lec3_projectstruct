﻿using AutoMapper;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.Common.Entities;

namespace Lection3.Common.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<UpdateTeamDTO, Team>();
            CreateMap<CreateTeamDTO, Team> ();
        }
    }
}
