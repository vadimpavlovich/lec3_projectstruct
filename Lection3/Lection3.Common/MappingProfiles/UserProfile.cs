﻿using AutoMapper;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.Common.Entities;

namespace Lection3.Common.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>(); 
            CreateMap<UpdateUserDTO, User>();
            CreateMap<CreateUserDTO, User>();
        }
    }
}
