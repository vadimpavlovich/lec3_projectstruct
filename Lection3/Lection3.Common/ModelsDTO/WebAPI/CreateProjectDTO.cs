﻿using System;

namespace Lection3.Common.ModelsDTO.WebAPI
{
    public class CreateProjectDTO
    {  
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    } 
}
