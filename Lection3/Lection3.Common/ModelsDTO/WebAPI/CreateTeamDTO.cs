﻿using System;

namespace Lection3.Common.ModelsDTO.WebAPI
{
    public class CreateTeamDTO
    { 
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; } 
    }
}
