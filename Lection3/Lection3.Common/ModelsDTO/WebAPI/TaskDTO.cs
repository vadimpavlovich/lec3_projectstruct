﻿using Lection3.Common.Enums;
using System;

namespace Lection3.Common.ModelsDTO.WebAPI
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskSatate State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; } 
    }
}
