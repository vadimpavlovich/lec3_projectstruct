﻿using System;

namespace Lection3.Common.ModelsDTO.WebAPI
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; } 
    }
}
