﻿using System;
using System.Collections.Generic;

namespace Lection3.ConsoleClient.Models.API
{
    public class Projects
    { 
        public int Id { get; set; } 
        public int AuthorId { get; set; } 
        public int TeamId { get; set; } 
        public string Name { get; set; } 
        public string Description { get; set; } 
        public DateTime Deadline { get; set; } 
        public DateTime CreatedAt { get; set; }


        public IEnumerable<Tasks> Tasks { get; set; }
        public Teams Team { get; set; }
        public Users Author { get; set; }
    }

}
