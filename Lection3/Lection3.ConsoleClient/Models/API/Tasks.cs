﻿using System; 

namespace Lection3.ConsoleClient.Models.API
{
    public class Tasks
    { 
        public int Id { get; set; } 
        public int ProjectId { get; set; } 
        public int PerformerId { get; set; } 
        public string Name { get; set; } 
        public string Description { get; set; } 
        public TaskSatete State { get; set; } 
        public DateTime CreatedAt { get; set; } 
        public DateTime? FinishedAt { get; set; }
        public Users Performer { get; set; } 
    }
}
