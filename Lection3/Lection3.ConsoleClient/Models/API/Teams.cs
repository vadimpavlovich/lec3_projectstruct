﻿using System;
using System.Collections.Generic; 

namespace Lection3.ConsoleClient.Models.API
{
    public class Teams
    { 
        public int Id { get; set; } 
        public string Name { get; set; } 
        public DateTime CreatedAt { get; set; }

        public IEnumerable<Users> Users { get; set; }
    }
}
