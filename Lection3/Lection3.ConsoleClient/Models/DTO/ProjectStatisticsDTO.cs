﻿using Lection3.ConsoleClient.Models.API; 

namespace Lection3.ConsoleClient.Models.DTO
{
    public class ProjectStatisticsDTO
    {
        /// <summary>
        /// Проект
        /// </summary>
        public Projects Project { get; set; }
        /// <summary>
        /// Найдовший таск проекту (за описом)
        /// </summary>
        public Tasks LongestDescriptionTask { get; set; }
        /// <summary>
        /// Найкоротший таск проекту (по імені)
        /// </summary>
        public Tasks ShortestNameTask { get; set; }
        /// <summary>
        /// Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3 
        /// </summary> 
        public int? UsersCntWithCondition { get; set; }
    }
}
