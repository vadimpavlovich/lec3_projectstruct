﻿using Lection3.ConsoleClient.Models.API;
using System.Collections.Generic;

namespace Lection3.ConsoleClient.Models.DTO
{
    public class TeamsSortedDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Users> Users { get; set; }
    }
}
