﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lection3.ConsoleClient.Models.DTO
{
    public class UserTasksFinishedDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
