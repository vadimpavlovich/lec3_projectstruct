﻿using Lection3.ConsoleClient.Models.API; 
using System.Collections.Generic; 

namespace Lection3.ConsoleClient.Models.DTO
{
    public class UsersWithTasksDTO : Users
    {
        public List<Tasks> Tasks { get; set; }
    }
}
