﻿using System; 

namespace Lection3.ConsoleClient.Models.Interface
{  
    public class GetProjectStatisticsMenu : IState
    {        
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Отримати статистику всіх проектів (Введіть quit для виходу): ");
         
            var res = Program.service.GetProjectStatistics();
            foreach (var stat in res)
            {
                Console.WriteLine($"Проект:                                             {stat.Project.Name}");
                if (stat.LongestDescriptionTask == null)
                    Console.WriteLine($"Найдовший таск проекту (за описом):                 В проекті відсутні таски");
                else
                    Console.WriteLine($"Найдовший таск проекту (за описом):                 {stat.LongestDescriptionTask.Name}");
                if (stat.ShortestNameTask == null)
                    Console.WriteLine($"Найкоротший таск проекту (по імені):                 В проекті відсутні таски");
                else
                    Console.WriteLine($"Найкоротший таск проекту (по імені):                {stat.ShortestNameTask.Name}");
                Console.WriteLine($"Загальна кількість користувачів в команді проекту:  {stat.UsersCntWithCondition}"); 
            } 

            ColorConsole.GreenColorOutput("Натисніть Enter щоб повернутись до головного меню"); 
            Console.ReadLine();
            Console.Clear(); 
            return new MainMenu(); 
        }
    }
}
