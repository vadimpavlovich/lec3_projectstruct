﻿using System;

namespace Lection3.ConsoleClient.Models.Interface
{
    public class GetUserListWithTasksMenu : IState
    {
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Отримати список користувачів за алфавітом з відсортованими тасками (Введіть quit для виходу): ");
            var result = Program.service.GetUserListWithTasks();

            Console.WriteLine("   User Id      Tasks count         User ");

            foreach (var item in result)
            {
                Console.WriteLine($"      {item.Id.ToString("D3")}         {item.Tasks.Count.ToString("D2")}           {item.FirstName} {item.LastName}");
            }

            ColorConsole.GreenColorOutput("Натисніть Enter щоб повернутись до головного меню");
            Console.ReadLine();
            Console.Clear();
            return new MainMenu();
        }
    }
}
