﻿using System; 

namespace Lection3.ConsoleClient.Models.Interface
{  
    public class GetUserStatisticsMenu : IState
    {      
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Отримати статистику користувача (Введіть quit для виходу): ");
            int? userId = MenuHelpers.ReadCorrectUserIdFromInput();
            if (userId == null)
                return new MainMenu();

            var res = Program.service.GetUserStatistics(userId.Value);

            double days = 0;
            if (res.LongestTask.FinishedAt != null)
                days = res.LongestTask.FinishedAt.Value.Subtract(res.LongestTask.CreatedAt).TotalDays;
            else
                days = DateTime.Now.Subtract(res.LongestTask.CreatedAt).TotalDays;  

            Console.WriteLine($"Користувач:                  {res.User.LastName} {res.User.FirstName} ");
            Console.WriteLine($"Останній проект:             {res.LastProject.Name} ");
            Console.WriteLine($"Тасків в остнньому проекті:  {res.TasksInLastProject} ");
            Console.WriteLine($"Кількість відмінених тасків: {res.CanceledTasksCount} ");
            Console.WriteLine($"Найдовший зачасом таск:      {res.LongestTask.Name} ({(int)days} днів)"); 

            ColorConsole.GreenColorOutput("Натисніть Enter щоб повернутись до головного меню"); 
            Console.ReadLine();
            Console.Clear(); 
            return new MainMenu(); 
        }
    }
}
