﻿using System; 

namespace Lection3.ConsoleClient.Models.Interface
{  
    public class GetUserTaskFinishedThisYearMenu : IState
    {
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Отримати список з колекції тасків, які виконані в поточному році для користувача (Введіть quit для виходу): ");
            int? userId = MenuHelpers.ReadCorrectUserIdFromInput();
            if (userId == null)
                return new MainMenu();

            var result = Program.service.GetUserTaskFinishedThisYear(userId.Value);

            if (result.Count == 0)
                Console.WriteLine($"У користувача відсутні таски."); 
            else
                Console.WriteLine("   Task Id     Task Name ");

            foreach (var item in result)
            {
                Console.WriteLine($"      {item.Id}      {item.Name} ");
            } 

            ColorConsole.GreenColorOutput("Натисніть Enter щоб повернутись до головного меню"); 
            Console.ReadLine();
            Console.Clear(); 
            return new MainMenu(); 
        }
    }
}
