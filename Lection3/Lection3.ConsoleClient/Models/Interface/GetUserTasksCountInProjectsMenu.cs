﻿using System; 

namespace Lection3.ConsoleClient.Models.Interface
{  
    public class GetUserTasksCountInProjectsMenu : IState
    {
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Отримати кількість тасків користувача у проекті (Введіть quit для виходу): ");
            int? userId = MenuHelpers.ReadCorrectUserIdFromInput();
            if (userId == null)
                return new MainMenu();

            var result = Program.service.GetUserTasksCountInProjects(userId.Value);
            Console.WriteLine("   Id     Count          Project Name ");
            foreach (var item in result)
            {
                Console.WriteLine($"   {item.Key.Id}      {item.Value}          {item.Key.Name} ");
            } 

            ColorConsole.GreenColorOutput("Натисніть Enter щоб повернутись до головного меню"); 
            Console.ReadLine();
            Console.Clear(); 
            return new MainMenu(); 
        }
    }
}
