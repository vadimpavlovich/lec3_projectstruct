﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lection3.ConsoleClient.Models.Interface
{
    /// <summary>
    /// Головне меню
    /// </summary>
    public class MainMenu : MenuState
    {
        private Dictionary<int, MenuItem> _menus = new Dictionary<int, MenuItem>() {
                {1,  new MenuItem(){Text = "Отримати кількість тасків користувача у проекті"}},
                {2,  new MenuItem(){Text = "Отримати список тасків, призначених для користувача"}},
                {3,  new MenuItem(){Text = "Отримати список з колекції тасків, які виконані в поточному році для користувача"}},
                {4,  new MenuItem(){Text = "Отримати список з колекції команд, учасники яких старші 10 років"}},
                {5,  new MenuItem(){Text = "Отримати список користувачів за алфавітом з відсортованими тасками"}},
                {6,  new MenuItem(){Text = "Отримати статистику користувача"}},
                {7,  new MenuItem(){Text = "Отримати статистику всіх проектів"}},
                {8,  new MenuItem(){Text = "Вихід"}}
            };
        protected override Dictionary<int, MenuItem> Menus => _menus;

        protected override IState NextState(KeyValuePair<int, MenuItem> selectedMenu)
        {
            switch (selectedMenu.Key)
            {
                case 1:
                    Console.Clear();
                    return new GetUserTasksCountInProjectsMenu();
                case 2:
                    Console.Clear();
                    return new GetUserTaskWithConditionMenu();
                case 3:
                    Console.Clear();
                    return new GetUserTaskFinishedThisYearMenu();
                case 4:
                    Console.Clear();
                    return new GetTeamsWithConditionMenu();
                case 5:
                    Console.Clear();
                    return new GetUserListWithTasksMenu();
                case 6:
                    Console.Clear();
                    return new GetUserStatisticsMenu();
                case 7:
                    Console.Clear();
                    return new GetProjectStatisticsMenu();
                case 8:
                    Console.Clear();
                    return null;
                default:
                    Console.Clear();
                    return this;
            }
        }
    }
}