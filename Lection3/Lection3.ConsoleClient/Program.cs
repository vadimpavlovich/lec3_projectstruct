﻿using Lection3.ConsoleClient.Models.Interface;
using Lection3.ConsoleClient.Services;
using System;
using System.Text;

namespace Lection3.ConsoleClient
{
    class Program
    {
        public static MainService service { get; set; }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8; 
            service = new MainService();
            Console.WriteLine($"Привіт, {Environment.UserName}!");
            Console.WriteLine($"Дані завантажуються, будь-ласка зачекайте.");
            try
            {
                service.LoadData();
            }
            catch
            {
                Console.WriteLine("При завантаженні данних виникла помилка. Будь-ласка спробуйте пізніше.");
                Console.WriteLine("Натисніть Enter для виходу.");
                Environment.Exit(0);
            }
            IState startState = new MainMenu();
            while (startState != null) startState = startState.RunState();
            Console.Clear();
            Console.WriteLine($"До зустрічі, {Environment.UserName}!");
        }


    }
}
