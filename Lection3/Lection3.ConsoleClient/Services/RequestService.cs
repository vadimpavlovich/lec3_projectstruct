﻿using Lection3.ConsoleClient.Models.API; 
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lection3.ConsoleClient.Services
{
    public class RequestService
    {
        private string ApiUrl { get; set; }
        private HttpClient client { get; set; }
        public List<Projects> projects { get; set; }
        public List<Teams> teams { get; set; }
        public List<Users> users { get; set; }
        public List<Tasks> tasks { get; set; }
        public RequestService(string ApiUrl)
        {
            this.ApiUrl = ApiUrl;
            client = new HttpClient();
        }
        public RequestService()
        {
            this.ApiUrl = @"https://localhost:5001/api/";
            client = new HttpClient();
        }

        public void LoadData()
        {
            projects = GetProjects();
            tasks = GetTasks();
            teams = GetTeams();
            users = GetUsers();
        }
        public List<Projects> MergeData()
        {
            //Join Users to teams 
            teams = teams.GroupJoin(users,
                t => t.Id,
                u => u.TeamId,
                (tm, usrs) => new Teams
                {
                    Id = tm.Id,
                    Name = tm.Name,
                    CreatedAt = tm.CreatedAt,
                    Users = usrs
                }).ToList();

            //Join Performers to tasks
            tasks = tasks.Join(users,
                t => t.PerformerId,
                u => u.Id,
                (t, u) => new Tasks
                {
                    Id = t.Id,
                    ProjectId = t.ProjectId,
                    PerformerId = t.PerformerId,
                    Name = t.Name,
                    Description = t.Description,
                    State = t.State,
                    CreatedAt = t.CreatedAt,
                    FinishedAt = t.FinishedAt,
                    Performer = u
                }).ToList();

            //Join Tasks to Projects also add team and author
            projects = projects.GroupJoin(tasks,
                p => p.Id,
                t => t.ProjectId,
                (prj, tasks) => new Projects
                {
                    Id = prj.Id,
                    AuthorId = prj.AuthorId,
                    TeamId = prj.TeamId,
                    Name = prj.Name,
                    Description = prj.Description,
                    Deadline = prj.Deadline,
                    CreatedAt = prj.CreatedAt,
                    Tasks = tasks,
                    Team = teams.FirstOrDefault(x => x.Id == prj.TeamId),
                    Author = users.FirstOrDefault(x => x.Id == prj.AuthorId)
                }).ToList();
            return projects;
        }
        private string Load(string url)
        {
            var webRequest = new HttpRequestMessage(HttpMethod.Get, url);
            var response = client.Send(webRequest);
            using var reader = new StreamReader(response.Content.ReadAsStream());
            if (response.IsSuccessStatusCode)
                return reader.ReadToEnd();
            else
                throw new Exception("GetProjects request is unsuccessful.");
        }
        private List<Projects> GetProjects()
        {
            var result = Load(ApiUrl + "projects");
            return JsonConvert.DeserializeObject<List<Projects>>(result);
        }
        private Projects GetProjectById(int id)
        {
            var result = Load(ApiUrl + $"projects/{id}");
            return JsonConvert.DeserializeObject<Projects>(result);
        }

        private List<Users> GetUsers()
        {
            var result = Load(ApiUrl + "users");
            return JsonConvert.DeserializeObject<List<Users>>(result);
        }
        private Users GetUserById(int id)
        {
            var result = Load(ApiUrl + $"users/{id}");
            return JsonConvert.DeserializeObject<Users>(result);
        }

        private List<Tasks> GetTasks()
        {
            var result = Load(ApiUrl + "tasks");
            return JsonConvert.DeserializeObject<List<Tasks>>(result);
        }
        private Tasks GetTaskById(int id)
        {
            var result = Load(ApiUrl + $"tasks/{id}");
            return JsonConvert.DeserializeObject<Tasks>(result);
        }
        private List<Teams> GetTeams()
        {
            var result = Load(ApiUrl + "teams");
            return JsonConvert.DeserializeObject<List<Teams>>(result);
        }
        private Teams GetTeamsById(int id)
        {
            var result = Load(ApiUrl + $"teams/{id}");
            return JsonConvert.DeserializeObject<Teams>(result);
        }
    }
}
