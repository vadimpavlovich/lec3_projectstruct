﻿using Lection3.Common.Interfaces;
using Lection3.DAL.Interfaces; 
using System.Collections.Generic;
using System.Linq; 

namespace Lection3.DAL.Context
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class, BaseEntity
    { 
        internal List<TEntity> entities;

        public BaseRepository(List<TEntity> rawData)
        {
            this.entities = rawData;
        } 
        private int GenerateNewId() 
        {
            return entities.Max(x => x.Id)+1;
        }
        public TEntity Add(TEntity entity)
        {
            entity.Id = GenerateNewId();
            entities.Add(entity);
            return entity;
        } 
        public void Delete(int id)
        {
            if (IsExist(id))
            {
                var itemToRemove = entities.FirstOrDefault(r => r.Id == id);
                entities.Remove(itemToRemove);
            }
        }
        public bool IsExist(int id) 
        {
            return entities.Any(r => r.Id == id);
        }
        public TEntity GetByID(int id)
        { 
            return entities.FirstOrDefault(r => r.Id == id);
        } 
        public List<TEntity> GetList()
        {
            return entities;
        } 
        public void Update(int id, TEntity entity)
        {
            if (IsExist(id))
            {
                entity.Id = id;
                var index = entities.FindIndex(r => r.Id == id);
                entities[index] = entity; 
            }
        } 
    }

}
