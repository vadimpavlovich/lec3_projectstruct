﻿using Lection3.Common.Entities;
using Lection3.DAL.Interfaces;
using Task = Lection3.Common.Entities.Task;

namespace Lection3.DAL.Context
{
    public class UnitOfWork : IUnitOfWork
    {
        private BaseRepository<Project> _projects;
        private BaseRepository<Team> _teams;
        private BaseRepository<Task> _tasks;
        private BaseRepository<User> _users;
        private InitialData _data;
        public UnitOfWork(InitialData data)
        {
            _data = data;
        } 
        public UnitOfWork()
        {
            _data = new InitialData();
        }
        public IRepository<Project> Projects
        {
            get
            {
                return _projects ??
                    (_projects = new BaseRepository<Project>(_data.Projects));
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                return _teams ??  (_teams = new BaseRepository<Team>(_data.Teams));
            }
        }
        public IRepository<User> Users
        {
            get
            {
                return _users ?? (_users = new BaseRepository<User>(_data.Users));
            }
        }
        public IRepository<Task> Tasks
        {
            get
            {
                return _tasks ?? (_tasks = new BaseRepository<Task>(_data.Tasks));
            }
        } 
    }
}
