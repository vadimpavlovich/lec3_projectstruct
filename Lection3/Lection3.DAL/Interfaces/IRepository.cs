﻿using System.Collections.Generic;

namespace Lection3.DAL.Interfaces
{
    public interface IRepository<Entity> where Entity : class
    { 
        List<Entity> GetList( );
        Entity GetByID(int id);
        Entity Add(Entity entity);
        void Update(int id, Entity entity);
        void Delete(int id);
        bool IsExist(int id); 
    }
}
