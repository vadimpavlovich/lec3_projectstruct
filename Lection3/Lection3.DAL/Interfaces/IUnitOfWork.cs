﻿using Lection3.Common.Entities;
using Task = Lection3.Common.Entities.Task;

namespace Lection3.DAL.Interfaces
{

    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<Team> Teams { get; }
        IRepository<Task> Tasks { get; }
        IRepository<User> Users { get; }  
    }

}
