﻿using AutoMapper;
using Lection3.Common.Entities;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Lection3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ProjectsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProjectDTO>>> GetProjectsList()
        {
            List<Project> projects = unitOfWork.Projects.GetList();
            return Ok(mapper.Map<List<ProjectDTO>>(projects));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectByID(int id)
        {
            if (!unitOfWork.Projects.IsExist(id))
                return NotFound();
            Project project = unitOfWork.Projects.GetByID(id);
            return Ok(mapper.Map<ProjectDTO>(project));
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> CreateProject([FromBody] CreateProjectDTO project)
        {
            Project newProject = unitOfWork.Projects.Add(mapper.Map<Project>(project));
            return Created("", mapper.Map<ProjectDTO>(newProject));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ProjectDTO>> UpdateProject(int id,[FromBody] UpdateProjectDTO project)
        {
            unitOfWork.Projects.Update(id,mapper.Map<Project>(project));
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ProjectDTO>> DeleteProject(int id)
        {
            unitOfWork.Projects.Delete(id);
            return NoContent();
        } 
    }
}
