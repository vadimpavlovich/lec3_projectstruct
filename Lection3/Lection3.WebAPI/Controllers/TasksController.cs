﻿using AutoMapper;
using Lection3.Common.Entities;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
 
namespace Lection3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public TasksController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<TaskDTO>>> GetTasksList()
        {
            var Tasks = unitOfWork.Tasks.GetList();
            return Ok(mapper.Map<List<TaskDTO>>(Tasks));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetTaskByID(int id)
        {
            if (!unitOfWork.Tasks.IsExist(id))
                return NotFound();
            var Task = unitOfWork.Tasks.GetByID(id);
            return Ok(mapper.Map<TaskDTO>(Task));
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> CreateTask([FromBody] CreateTaskDTO Task)
        {
            var newTask = unitOfWork.Tasks.Add(mapper.Map<Common.Entities.Task>(Task));
            return Created("", mapper.Map<TaskDTO>(newTask));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TaskDTO>> UpdateTask(int id, [FromBody] UpdateTaskDTO Task)
        {
            unitOfWork.Tasks.Update(id, mapper.Map<Common.Entities.Task>(Task));
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TaskDTO>> DeleteTask(int id)
        {
            unitOfWork.Tasks.Delete(id);
            return NoContent();
        }
    }
}
