﻿using AutoMapper;
using Lection3.Common.Entities;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Lection3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public TeamsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<TeamDTO>>> GetTeamsList()
        {
            List<Team> Teams = unitOfWork.Teams.GetList();
            return Ok(mapper.Map<List<TeamDTO>>(Teams));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetTeamByID(int id)
        {
            if (!unitOfWork.Teams.IsExist(id))
                return NotFound();
            Team Team = unitOfWork.Teams.GetByID(id);
            return Ok(mapper.Map<TeamDTO>(Team));
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] CreateTeamDTO Team)
        {
            Team newTeam = unitOfWork.Teams.Add(mapper.Map<Team>(Team));
            return Created("", mapper.Map<TeamDTO>(newTeam));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TeamDTO>> UpdateTeam(int id,[FromBody] UpdateTeamDTO Team)
        {
            unitOfWork.Teams.Update(id,mapper.Map<Team>(Team));
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TeamDTO>> DeleteTeam(int id)
        {
            unitOfWork.Teams.Delete(id);
            return NoContent();
        } 
    }
}
