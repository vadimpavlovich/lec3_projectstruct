﻿using AutoMapper;
using Lection3.Common.Entities;
using Lection3.Common.ModelsDTO.WebAPI;
using Lection3.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Lection3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public UsersController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserDTO>>> GetUsersList()
        {
            List<User> Users = unitOfWork.Users.GetList();
            return Ok(mapper.Map<List<UserDTO>>(Users));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUserByID(int id)
        {
            if (!unitOfWork.Users.IsExist(id))
                return NotFound();
            User User = unitOfWork.Users.GetByID(id);
            return Ok(mapper.Map<UserDTO>(User));
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> CreateUser([FromBody] CreateUserDTO User)
        {
            User newUser = unitOfWork.Users.Add(mapper.Map<User>(User));
            return Created("", mapper.Map<UserDTO>(newUser));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<UserDTO>> UpdateUser(int id, [FromBody] UpdateUserDTO User)
        {
            unitOfWork.Users.Update(id, mapper.Map<User>(User));
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<UserDTO>> DeleteUser(int id)
        {
            unitOfWork.Users.Delete(id);
            return NoContent();
        }
    }
}
