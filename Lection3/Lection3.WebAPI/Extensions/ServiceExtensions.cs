﻿using AutoMapper;
using Lection3.Common.Entities;
using Lection3.Common.MappingProfiles;
using Lection3.DAL.Context;
using Lection3.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Reflection; 

namespace Lection3.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }

        public static void LoadInitialData(this IServiceCollection services)
        {
            IConfiguration config = new ConfigurationBuilder()
                                              .SetBasePath(Directory.GetCurrentDirectory())
                                              .AddJsonFile("repository.json", optional: true).Build();
            var initialData = config.GetSection("InitialData").Get<InitialData>();
            services.AddSingleton<IUnitOfWork> (new UnitOfWork(initialData)); 
        }
    }
}
